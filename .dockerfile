#Author: Leonardo Conzelmann
# Pull l'image minio
FROM minio/mc 
#Set les variables qui seront passer dans le CMD.
ENV S3_URL="url"
ENV S3_ACCESS_TOKEN="token"
ENV S3_ACCESS_KEY="key"
EXPOSE 9001
# Je me dit que je pourrait faire un COPY ici
# et mettre mon fichier backup.sh, maybe.
RUN \
    mc mb minioTim/backup && \ 
    mc cp -r 5ika minioTim/backup
#CMD est une commande passer en tant qu'argument au lancement du container
CMD mc config host add minioTim ${S3_URL} ${S3_ACCESS_TOKEN} ${S3_ACCESS_KEY} --api S3v4
#conseil de Robin prendre une image alpine et faire un wget dans les run
