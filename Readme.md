# TP server S_3

A rendre le 12 mai 2019.

## But et marche à suivre:

But: Faire des backup des données sur un serveur S3 distant

1. Comprendre comment minio marche.

1.1 Installer minio sur sa machine suivre la doc @https://docs.min.io/docs/minio-client-complete-guide.html

2. Etablir une connection au serveur:

```bash
mc config host add ALIAS_DU_SERVEUR $urlMinio $accessKey $secretKey --api S3v4
#il faut set les variables d'environnement
export accessKey=MY_ACCESS_KEY
export secretKey=MY_SECRET_KEY
export urlMinio=MY_URL
```

3. Creer un bucket sur le serveur:

```bash
mc mb ALIAS_DU_SERVEUR/NOM_DU_BUCKET
```

4.  Copier notre fichier dans le bucket qui est sur le serveur.

```bash
mc cp -r monfichier.txt ALIAS_DU_SERVEUR/NOM_DU_BUCKET
```


# Partie 2 

1. Créer un dockerfile et pull l'image dockerhub de minio

```Dockerfile
FROM minio/mc

ENV S3_URL=""
ENV S3_ACCESS_TOKEN=""
ENV S3_ACCESS_KEY=""
```

2. Lancer les commande voulu dans notre Dockerfile

```Dockerfile
# '\' pour faire des retours de ligne
RUN \
    mc mb minioTim/backup && \
    mc cp -r 5ika minioTim/backup

CMD mc config host add minioTim ${S3_URL} ${S3_ACCESS_TOKEN} ${S3_ACCESS_KEY} --api S3v4
```